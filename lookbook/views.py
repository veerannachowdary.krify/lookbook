# from .models import StudentModel
# from .serializer import StudentSerializer
# from rest_framework import mixins, generics
from django.http import HttpResponse
from django.conf import settings

# reportlab imports
import reportlab as reportlab
from reportlab.pdfgen import canvas
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import A4, A2,cm, landscape, letter
from reportlab.pdfbase import pdfmetrics
from reportlab.lib import colors
from reportlab.pdfbase import pdfform
from reportlab.lib.colors import magenta, pink, blue, green
from reportlab.lib.units import inch
from reportlab.platypus import Paragraph, Table, TableStyle, Image
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
from reportlab.lib.styles import getSampleStyleSheet
from PIL import Image,ImageOps
from reportlab.lib.styles import ParagraphStyle
import requests
import datetime
from io import BytesIO
from reportlab.lib.utils import ImageReader
from reportlab.graphics import shapes
from reportlab.graphics.charts.textlabels import Label
import os.path
import textwrap 
reportlab.rl_config.TTFSearchPath.append(str(settings.BASE_DIR))
pdfmetrics.registerFont(TTFont('BarlowBold', 'Barlow-Bold.ttf'))
pdfmetrics.registerFont(TTFont('BarlowRegular','Barlow-Regular.ttf'))
pdfmetrics.registerFont(TTFont('GilroyBold','Gilroy-Bold.ttf'))
# class Studentgetinsert(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
#     queryset = StudentModel.objects.all()
#     serializer_class = StudentSerializer

#     def get(self, request):
#         return self.list(request)

#     def post(self, request):
#         return self.create(request)


# class Studentupdatedel(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):
#     queryset = StudentModel.objects.all()
#     serializer_class = StudentSerializer

#     def get(self, request, pk):
#         return self.retrieve(request, pk)

#     def put(self, request, pk):
#         return self.update(request, pk)

#     def delete(self, request, pk):
#         return self.destroy(request, pk)


def FourPerPageP(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BP = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'Potrait.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=letter)
    pdf.setTitle("Fourperpage_potriat_Lookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(275, 730, "[Sample]")
    pdf.setFont("Courier", 10)
    pdf.drawImage(BP, 20, 20, 600, 700)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1, 1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    perpage = 6
    perpage_ = 0
    perrow = 2
    perrow_ = 0
    k = 0
    n = 0
    for j in range(6):
        perrow_ = 0
        k = 0
        n = 0
        for i in range(0, 4):
            if(perrow_ >= perrow):
                perrow_ = 0
                k = k+340
                n = 0
            else:
                pass
            pdf.drawImage(fn, 15, 750, 30, 30)
            pdf.line(60, 750, 600, 750)
            pdf.drawImage(ir, 35+(n*280), 480-k, 150, 225)
            pdf.setFont("Courier-Bold", 18)
            pdf.setFillColor("#ff1552")
            pdf.roundRect(85+(n*280), 440-k, 150, 25, 6, stroke=1, fill=1)
            pdf.setFillColor("White")
            pdf.setFont("GilroyBold", 12)
            # pdf.drawString(135+(n*280), 455-k, "veeru")
            fname = "Jess"
            value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
            wrapper = textwrap.TextWrapper(width=25) 
            word_list = wrapper.wrap(text=value)
            string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 12)
            pdf.drawString((85+(n*280)+(int(150)/2))-int(string_width)/2, 445-k, fname)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowBold", 8)
            pdf.drawString(200+(n*280), 700-k, "Age:23")
            pdf.drawString(250+(n*280), 700-k, "Height:5'10")
            pdf.drawString(200+(n*280), 685-k, "Weight:68")
            pdf.drawString(250+(n*280), 685-k, "Age:23")
            pdf.drawString(200+(n*280), 670-k, "Height:5'10")
            pdf.drawString(250+(n*280), 670-k, "Weight:68")
            pdf.drawString(200+(n*280), 655-k, "Age:23")
            pdf.drawString(250+(n*280), 655-k, "Height:5'10")
            pdf.drawString(200+(n*280), 640-k, "Weight:68")
            pdf.roundRect(200+(n*280),
                          480-k, 110, 150, 6, stroke=1, fill=0)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowRegular", 8)
            y_axis=620-k
            for element in word_list:
                pdf.drawString(210+(n*280),y_axis,element)
                y_axis=y_axis-15
            perrow_ = perrow_+1
            n = n + 1
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def FourPerPageL(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="FourperpageL.pdf"'
    # pdfmetrics.registerFont(TTFont('Gilroy', 'Gilroy.ttf'))
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BL = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'landscape.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=landscape(A4))
    pdf.setTitle("Fourperpage_landscape_Lookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(375, 530, "[Sample]")
    pdf.setFont("Courier", 8)
    pdf.drawImage(BL, 75, 75, 700, 450)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1, 1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    perpage = 6
    perpage_ = 0
    perrow = 4
    perrow_ = 0
    k = 0
    n = 0
    for j in range(0, 6):
        for i in range(0, 4):
            if(perrow_ >= perrow):
                perrow_ = 0
                k = k+200
                n = 0
            else:
                pass
            pdf.drawImage(fn, 15, 550, 30, 30)
            pdf.line(60, 550, 800, 550)
            pdf.drawImage(ir, 60+(n*90), 310, 180, 220)
            # pdf.setFont("GilroyBold", 12)
            pdf.setFillColor("#ff1552")
            pdf.roundRect(75+(n*90), 285, 145, 20, 6, stroke=1, fill=1)
            pdf.setFillColor("White")
            pdf.setFont("GilroyBold", 12)
            # pdf.drawString(100+(n*90), 315, "Veeru")
            fname = "Jess"
            value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long."""
            wrapper = textwrap.TextWrapper(width=35) 
            word_list = wrapper.wrap(text=value)
            string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 12)
            pdf.drawString((75+(n*90)+(int(145)/2))-int(string_width)/2, 290, fname)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowBold", 10)
            pdf.drawString(60+(n*90),265, "Age:23")
            pdf.drawString(120+(n*90), 265, "Height:5'10")
            pdf.drawString(190+(n*90), 265, "Weight:68")
            pdf.drawString(60+(n*90), 240, "Age:23")
            pdf.drawString(120+(n*90), 240, "Height:5'10")
            pdf.drawString(190+(n*90), 240, "Weight:68")
            pdf.drawString(60+(n*90), 215, "Age:23")
            pdf.drawString(120+(n*90), 215, "Color:White")
            pdf.drawString(190+(n*90), 215, "Weight:68")
            pdf.roundRect(60+(n*90),
                          100, 175, 100, 6, stroke=1, fill=0)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowRegular", 10)
            y_axis=185
            for element in word_list:
                pdf.drawString(70+(n*90),y_axis,element)
                y_axis=y_axis-15
            perrow_ = perrow_+1
            n = n + 2.1
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def SixPerPage(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="Sixperpage.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BL = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'landscape.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=landscape(A4))
    pdf.setPageSize((600, 600))
    pdf.setTitle("LookBook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(245, 500, "[Sample]")
    pdf.setFont("Courier", 8)
    pdf.drawImage(BL, 40, 40, 500, 450)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (2, 2, 2, 2)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    story = []
    perpage = 6
    perpage_ = 0
    perrow = 3
    perrow_ = 0
    k = 0
    n = 0
    for j in range(6):
        perrow_ = 0
        k = 0
        n = 0
        for i in range(0, 6):
            if(perrow_ >= perrow):
                perrow_ = 0
                k = k+225
                n = 0
            else:
                pass
            pdf.drawImage(fn, 15, 550, 30, 30)
            pdf.line(60, 550, 575, 550)
            pdf.setFont("BarlowBold", 8)
            pdf.drawImage(ir, 50+(n*185), 340 - k, 100, 150)
            pdf.drawString(155+(n*185), 485-k, "Age:23")
            pdf.drawString(155+(n*185), 470-k, "Number:789")
            pdf.drawString(155+(n*185), 455-k, "Name:veeru")
            pdf.drawString(155+(n*185), 440-k, "Height:175")
            pdf.drawString(155+(n*185), 425-k, "Weight:68")
            pdf.drawString(155+(n*185), 410-k, "Color:white")
            # pdf.drawString(160+(n*185), 435-k, "Hello")
            # textobject = pdf.beginText()
            # textobject.setTextOrigin(155, 430)
            # textobject.textLine('Reportlab cursor demo')
            # pdf.drawText(textobject)
            # pdf.roundRect(155+(n*185),
            #               385-k, 50, 60, 6, stroke=1, fill=0)
            pdf.roundRect(155+(n*185),
                          340-k, 50, 60, 6, stroke=1, fill=0)
            pdf.setFont("GilroyBold", 12)
            pdf.setFillColor("#ff1552")
            pdf.roundRect(60+(n*185), 315-k, 140, 20, 6, stroke=1, fill=1)
            pdf.setFillColor("white")
            # pdf.drawString(120+(n*185), 320-k, 'Veeru')
            fname = "Jess"
            value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
            wrapper = textwrap.TextWrapper(width=35) 
            word_list = wrapper.wrap(text=value)
            string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 12)
            pdf.drawString((60+(n*185)+(int(140)/2))-int(string_width)/2, 320-k, fname)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("Courier", 8)
            perrow_ = perrow_+1
            n = n + 1
        # textobject = pdf.beginText(2*cm, 29.7 * cm - 2 * cm)
        # for line in my_text.splitlines(False):
        #     textobject.textLine(line.rstrip())
        # pdf.drawText(textobject)
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def TwoperpageOL(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="TwoperpageOL.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BL = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'landscape.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=landscape(A4))
    pdf.setTitle("OneperpageLookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(375, 530, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BL, 75, 75, 700, 450)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1,1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    perpage = 6
    perpage_ = 0
    perrow = 2
    perrow_ = 0
    k = 0
    n = 0
    for j in range(0, 6):
        for i in range(0, 2):
            if(perrow_ >= perrow):
                perrow_ = 0
                k = k+200
                n = 0
            else:
                pass
            pdf.drawImage(fn, 15, 550, 30, 30)
            pdf.line(60, 550, 825, 550)
            pdf.drawImage(ir, 20+(n*190), 175, 175, 250)
            pdf.setFont("GilroyBold", 14)
            pdf.setFillColor('#ff1552')
            # pdf.roundRect(225+(n*190), 400, 200, 25, 6, stroke=1, fill=1)
            pdf.roundRect(65+(n*190), 140, 300, 25, 6, stroke=1, fill=1)
            pdf.setFillColor("white")
            # pdf.drawString(305+(n*190), 405, "Veeru")
            fname = "Jess"
            value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
            wrapper = textwrap.TextWrapper(width=35) 
            word_list = wrapper.wrap(text=value)
            string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
            pdf.drawString((65+(n*190)+(int(300)/2))-int(string_width)/2, 150, fname)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowBold", 10)
            pdf.drawString(210+(n*190), 415, "Age:23")
            pdf.drawString(210+(n*190), 400, "Weight:68")
            pdf.drawString(210+(n*190), 385, "Height:5'10")
            pdf.drawString(210+(n*190), 370, "Number:7985641230")
            pdf.drawString(210+(n*190), 355, "Color:green")
            pdf.drawString(210+(n*190), 340, "Dress:jeans")
            pdf.drawString(210+(n*190), 325, "DOB:23-05-2005")
            pdf.drawString(210+(n*190), 310, "Age:23")
            pdf.roundRect(210+(n*190),
                          175, 200, 130, 6, stroke=1, fill=0)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowRegular", 12)
            y_axis=285
            for element in word_list:
                pdf.drawString(215+(n*190),y_axis,element)
                y_axis=y_axis-15
            perrow_ = perrow_+1
            n = n + 2.1
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def TwoperpageTL(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    #response['Content-Disposition'] = f'inline; filename="TwoperpageTL.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BL = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'landscape.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=landscape(A4))
    pdf.setTitle("TwoperpageLookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(375, 530, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BL, 75, 75, 700, 450)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (2, 2, 2, 2)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    perpage = 6
    perpage_ = 0
    perrow = 2
    perrow_ = 0
    k = 0
    n = 0
    for j in range(0, 6):
        for i in range(0, 2):
            if(perrow_ >= perrow):
                perrow_ = 0
                k = k+225
                n = 0
            else:
                pass
            pdf.drawImage(fn, 15, 550, 30, 30)
            pdf.line(60, 550, 800, 550)
            pdf.drawImage(ir, 40+(n*180), 340,180, 200)
            pdf.drawImage(ir, 230+(n*180), 340, 180, 200)
            pdf.setFont("GilroyBold", 14)
            pdf.setFillColor("#ff1552")
            pdf.roundRect(100+(n*180), 310, 250, 20, 6, stroke=1, fill=1)
            pdf.setFillColor("white")
            # pdf.drawString(210+(n*180), 255, "Veeru")
            fname = "Jess"
            value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
            wrapper = textwrap.TextWrapper(width=70) 
            word_list = wrapper.wrap(text=value)
            string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
            pdf.drawString((100+(n*180)+(int(250)/2))-int(string_width)/2, 315, fname)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowBold", 12)
            pdf.drawString(40+(n*180), 280, "Age:23")
            pdf.drawString(175+(n*180), 280, "Height:5'10")
            pdf.drawString(345+(n*180), 280, "Weight:68")
            pdf.drawString(40+(n*180), 250, "Age:23")
            pdf.drawString(175+(n*180), 250, "Height:5'10")
            pdf.drawString(345+(n*180), 250, "Weight:68")
            pdf.drawString(40+(n*180), 220, "Age:23")
            pdf.drawString(175+(n*180), 220, "Height:5'10")
            pdf.drawString(345+(n*180), 220, "Weight:68")
            pdf.roundRect(40+(n*180),
                          65, 380, 135, 6, stroke=1, fill=0)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowRegular", 12)
            y_axis=180
            for element in word_list:
                pdf.drawString(45+(n*180),y_axis,element)
                y_axis=y_axis-15
            perrow_ = perrow_+1
            n = n+2.2
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def TwoperpageOP(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    #response['Content-Disposition'] = f'inline; filename="TwoperpageOP.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BP = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'Potrait.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=letter)
    pdf.setTitle("OneperpageLookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(275, 730, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BP, 20, 20, 600, 700)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1, 1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    perpage = 6
    perpage_ = 0
    perrow = 1
    perrow_ = 0
    k = 0
    n = 0
    for j in range(0, 2):
        for i in range(0, 2):
            if(perrow_ > perrow):
                perrow_ = 0
                n = 0
            else:
                pass
            pdf.drawImage(fn, 15, 740, 30, 30)
            pdf.line(60, 740, 600, 740)
            pdf.drawImage(ir, 30, 400-k, 200, 300)
            pdf.setFillColor("#ff1552")
            pdf.setFont("GilroyBold", 14)
            pdf.roundRect(300, 680-k, 250, 20, 6, stroke=1, fill=1)
            pdf.setFillColor("white")
            # pdf.drawString(400, 685-k, "Veeru")
            fname = "Jess"
            value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
            wrapper = textwrap.TextWrapper(width=60) 
            word_list = wrapper.wrap(text=value)
            string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
            pdf.drawString((300+(int(250)/2))-int(string_width)/2, 685-k, fname)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowBold", 12)
            pdf.drawString(245, 660-k, "Color:white")
            pdf.drawString(355, 660-k, "Name:veeru")
            pdf.drawString(460, 660-k, "Number:789654122")
            pdf.drawString(245, 610-k, "Color:white")
            pdf.drawString(355, 610-k, "Name:veeru")
            pdf.drawString(460, 610-k, "Number:789654122")
            pdf.drawString(245, 560-k, "Color:white")
            pdf.drawString(355, 560-k, "Name:veeru")
            pdf.drawString(460, 560-k, "Number:789654122")
            pdf.roundRect(245,
                          400-k, 335, 150, 6, stroke=1, fill=0)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowRegular", 12)
            y_axis=530-k
            for element in word_list:
                pdf.drawString(255,y_axis,element)
                y_axis=y_axis-15
            perrow_ = perrow_+1
            k = k+325
            n = n + 1
        k = 0
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def TwoperpageTP(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="TwoperpageOP.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BP = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'Potrait.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=letter)
    pdf.setTitle("TwoperpageLookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(275, 730, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BP, 20, 20, 600, 700)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1, 1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    perpage = 6
    perpage_ = 0
    perrow = 1
    perrow_ = 0
    k = 0
    n = 0
    for i in range(3):
        for i in range(0, 2):
            if(perrow_ > perrow):
                perrow_ = 0
                n = 0
            else:
                pass
            pdf.drawImage(fn, 15, 750, 30, 30)
            pdf.line(60, 750, 575, 750)
            pdf.drawImage(ir, 160, 520-k, 150, 225)
            pdf.drawImage(ir, 320, 520-k, 150, 225)
            pdf.setFont("GilroyBold", 14)
            pdf.setFillColor("#ff1552")
            pdf.roundRect(160, 490-k, 300, 20, 6, stroke=1, fill=1)
            pdf.setFillColor("white")
            # pdf.drawString(280, 495-k, "Veeru")
            fname = "Jess"
            value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long."""
            wrapper = textwrap.TextWrapper(width=40) 
            word_list = wrapper.wrap(text=value)
            string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
            pdf.drawString((160+(int(300)/2))-int(string_width)/2, 495-k, fname)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowBold", 12)
            pdf.drawString(100, 470-k, "Age:23")
            pdf.drawString(155, 470-k, "Height:5'10")
            pdf.drawString(230, 470-k, "Weight:68")
            pdf.drawString(100, 445-k, "Age:23")
            pdf.drawString(155, 445-k, "Height:5'10")
            pdf.drawString(230, 445-k, "Weight:68")
            pdf.drawString(100, 415-k, "Age:23")
            pdf.drawString(155, 415-k, "Height:5'10")
            pdf.drawString(230, 415-k, "Weight:68")
            pdf.roundRect(300,
                          410-k, 225, 75, 6, stroke=1, fill=0)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowRegular", 12)
            y_axis=470-k
            for element in word_list:
                pdf.drawString(310,y_axis,element)
                y_axis=y_axis-15
            perrow_ = perrow_+1
            k = k+350
            n = n + 1
        k = 0
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def ThreeperpageOL(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="TwoperpageOP.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BL = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'landscape.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=landscape(A4))
    pdf.setTitle("OneperpageLookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(375, 530, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BL, 75, 75, 700, 450)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1, 1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    perpage = 6
    perpage_ = 0
    perrow = 3
    perrow_ = 0
    k = 0
    n = 0
    for j in range(0, 6):
        for i in range(0, 3):
            if(perrow_ >= perrow):
                perrow_ = 0
                k = k+200
                n = 0
            else:
                pass
            pdf.drawImage(fn, 15, 550, 30, 30)
            pdf.line(60, 550, 800, 550)
            pdf.drawImage(ir, 50+(n*120), 300, 240, 240)
            pdf.setFont("GilroyBold", 14)
            pdf.setFillColor("#ff1552")
            pdf.roundRect(85+(n*120), 270, 170, 25, 6, stroke=1, fill=1)
            pdf.setFillColor("white")
            fname = "Jess"
            value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
            wrapper = textwrap.TextWrapper(width=50) 
            word_list = wrapper.wrap(text=value)
            string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
            pdf.drawString((85+(n*120)+(int(170)/2))-int(string_width)/2, 275, fname)
            # pdf.drawString(150+(n*120), 253, "Veeru")
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowBold", 12)
            pdf.drawString(55+(n*120), 250, "Age:23")
            pdf.drawString(120+(n*120), 250, "Height:5'10")
            pdf.drawString(220+(n*120), 250, "Weight:68")
            pdf.drawString(55+(n*120), 230, "Age:23")
            pdf.drawString(120+(n*120), 230, "Height:5'10")
            pdf.drawString(220+(n*120), 230, "Weight:68")
            pdf.drawString(55+(n*120), 210, "Age:23")
            pdf.drawString(120+(n*120), 210, "Height:5'10")
            pdf.drawString(220+(n*120), 210, "Weight:68")
            pdf.roundRect(50+(n*120),
                          85, 240, 100, 6, stroke=1, fill=0)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowRegular", 10)
            y_axis=165
            for element in word_list:
                pdf.drawString(55+(n*120),y_axis,element)
                y_axis=y_axis-15
            perrow_ = perrow_+1
            n = n + 2.1
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def ThreeperpageTL(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="TwoperpageOP.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BL = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'landscape.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=landscape(A4))
    pdf.setTitle("TwoperpageLookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(375, 530, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BL, 75, 75, 700, 450)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1, 1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    perpage = 6
    perpage_ = 0
    perrow = 3
    perrow_ = 0
    k = 0
    n = 0
    for j in range(0, 6):
        for i in range(0, 3):
            if(perrow_ >= perrow):
                perrow_ = 0
                k = k+200
                n = 0
            else:
                pass
            pdf.drawImage(fn, 15, 550, 30, 30)
            pdf.line(60, 550, 800, 550)
            pdf.drawImage(ir, 50+(n*120), 360, 120, 180)
            pdf.drawImage(ir, 175+(n*120), 360, 120, 180)
            pdf.setFont("GilroyBold", 14)
            pdf.setFillColor("#ff1552")
            pdf.roundRect(85+(n*120), 330, 165, 25, 6, stroke=1, fill=1)
            pdf.setFillColor("white")
            # pdf.drawString(155+(n*120), 290, "Veeru")
            fname = "Jess"
            value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
            wrapper = textwrap.TextWrapper(width=50) 
            word_list = wrapper.wrap(text=value)
            string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
            pdf.drawString((85+(n*120)+(int(165)/2))-int(string_width)/2, 340, fname)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowBold", 12)
            pdf.drawString(55+(n*120), 310, "Age:23")
            pdf.drawString(120+(n*120), 310, "Height:5'10")
            pdf.drawString(225+(n*120), 310, "Weight:68")
            pdf.drawString(55+(n*120), 285, "Age:23")
            pdf.drawString(120+(n*120), 285, "Height:5'10")
            pdf.drawString(225+(n*120), 285, "Weight:68")
            pdf.drawString(55+(n*120), 260, "Age:23")
            pdf.drawString(120+(n*120), 260, "Height:5'10")
            pdf.drawString(225+(n*120), 260, "Weight:68")
            pdf.roundRect(55+(n*120),
                          120, 240, 120, 6, stroke=1, fill=0)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowRegular", 10)
            y_axis=225
            for element in word_list:
                pdf.drawString(60+(n*120),y_axis,element)
                y_axis=y_axis-15
            perrow_ = perrow_+1
            n = n + 2.1
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def ThreeperpageOP(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="TwoperpageOP.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BP = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'Potrait.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=letter)
    pdf.setTitle("OneperpageLookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(275, 730, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BP, 20, 20, 600, 700)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1, 1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    perpage = 6
    perpage_ = 0
    perrow = 1
    perrow_ = 0
    k = 0
    n = 0
    for j in range(0, 2):
        for i in range(0, 3):
            if(perrow_ > perrow):
                perrow_ = 0
                n = 0
            else:
                pass
            pdf.drawImage(fn, 15, 740, 30, 30)
            pdf.line(60, 740, 600, 740)
            pdf.drawImage(ir, 50, 520-k, 120, 180)
            pdf.setFont("GilroyBold", 14)
            pdf.setFillColor("#ff1552")
            pdf.roundRect(260, 680-k, 250, 20, 6, stroke=1, fill=1)
            pdf.setFillColor("white")
            # pdf.drawString(360, 685-k, "Veeru")
            fname = "Jess"
            value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
            wrapper = textwrap.TextWrapper(width=60) 
            word_list = wrapper.wrap(text=value)
            string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
            pdf.drawString((260+(int(250)/2))-int(string_width)/2, 685-k, fname)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowBold", 12)
            pdf.drawString(220, 660-k, "Color:white")
            pdf.drawString(335, 660-k, "Name:veeru")
            pdf.drawString(440, 660-k, "Number:789654122")
            pdf.drawString(220, 645-k, "Color:white")
            pdf.drawString(335, 645-k, "Name:veeru")
            pdf.drawString(440, 645-k, "Number:789654122")
            pdf.drawString(220, 630-k, "Color:white")
            pdf.drawString(335, 630-k, "Name:veeru")
            pdf.drawString(440, 630-k, "Number:789654122")
            pdf.roundRect(220,
                          520-k, 340, 100, 6, stroke=1, fill=0)
            pdf.setFillColorRGB(0, 0, 0)
            pdf.setFont("BarlowRegular", 12)
            y_axis=600
            for element in word_list:
                pdf.drawString(230,y_axis-k,element)
                y_axis=y_axis-15
            perrow_ = perrow_+1
            k = k+220
            n = n + 1
        k = 0
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def OneperpageOL(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="TwoperpageOP.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BL = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'landscape.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=landscape(A4))
    pdf.setTitle("OneperpageLookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(375, 530, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BL, 75, 75, 700, 450)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1, 1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    perpage = 6
    perpage_ = 0
    perrow = 3
    perrow_ = 0
    k = 0
    n = 0
    for i in range(0, 6):
        pdf.drawImage(fn, 15, 550, 30, 30)
        pdf.line(60, 550, 775, 550)
        pdf.drawImage(ir, 90, 100, 300, 370)
        pdf.setFont("GilroyBold", 14)
        pdf.setFillColor("#ff1552")
        pdf.roundRect(425, 445, 320, 25, 6, stroke=1, fill=1)
        pdf.setFillColor("white")
        # print (str(settings.BASE_DIR))
        fname = "Jess"
        value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
        wrapper = textwrap.TextWrapper(width=50) 
        word_list = wrapper.wrap(text=value)
        string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
        pdf.drawString(585-int(string_width)/2, 455, fname)
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowBold", 12)
        pdf.drawString(425, 400, "Age:23")
        pdf.drawString(625, 400, "Height:5'10")
        pdf.drawString(425, 370, "Weight:68")
        pdf.drawString(625, 370, "Color:white")
        pdf.drawString(425, 340, "Name:veeru")
        pdf.drawString(625, 340, "Number:7896541223")
        pdf.roundRect(425,
                      100, 325, 200, 6, stroke=1, fill=0)
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowRegular", 14)
        y_axis=285
        for element in word_list:
            pdf.drawString(430,y_axis,element)
            y_axis=y_axis-15
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def OneperpageTL(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="TwoperpageOP.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BL = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'landscape.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=landscape(A4))
    pdf.setTitle("TwoperpageLookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(375, 530, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BL, 75, 75, 700, 450)
    pdf.showPage()
    color = "black"
    border = (2, 2, 2, 2)
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (2, 2, 2, 2)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    for i in range(0, 6):
        pdf.drawImage(fn, 15, 550, 30, 30)
        pdf.line(60, 550, 775, 550)
        pdf.drawImage(ir, 50, 150, 190, 300)
        pdf.drawImage(ir, 250, 150, 190, 300)
        pdf.setFont("GilroyBold", 14)
        pdf.setFillColor("#ff1552")
        pdf.roundRect(450, 420, 320, 30, 6, stroke=1, fill=1)
        pdf.setFillColor("white")               
        fname = "Jess"  
        value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
        wrapper = textwrap.TextWrapper(width=50) 
        word_list = wrapper.wrap(text=value)
        string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
        pdf.drawString(610-int(string_width)/2, 430, fname)
        # pdf.drawString(575, 430, "Veeru")
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowBold", 12)

        pdf.drawString(450, 390, "Age:23")
        pdf.drawString(650, 390, "Height:5'10")
        pdf.drawString(450, 360, "Weight:68")
        pdf.drawString(650, 360, "Color: white")
        pdf.drawString(450, 330, "Name:veeru")
        pdf.drawString(650, 330, "Number:7896541223")
        pdf.roundRect(450,
                      150, 325, 150, 6, stroke=1, fill=0)
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowRegular", 14)
        y_axis=285
        for element in word_list:
            pdf.drawString(455,y_axis,element)
            y_axis=y_axis-15
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response

def OneperpageThL(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="TwoperpageOP.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BL = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'landscape.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=landscape(A4))
    pdf.setTitle("ThreeperpageLookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(375, 530, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BL, 75, 75, 700, 450)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1, 1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    for i in range(0, 6):
        pdf.drawImage(fn, 15, 550, 30, 30)
        pdf.line(60, 550, 775, 550)
        pdf.drawImage(ir, 60, 240, 190, 300)
        pdf.drawImage(ir, 325, 240, 190, 300)
        pdf.drawImage(ir, 580, 240, 190, 300)
        pdf.setFont("GilroyBold", 14)
        pdf.setFillColor("#ff1552")
        pdf.roundRect(190, 215, 450, 20, 6, stroke=1, fill=1)
        pdf.setFillColor("white")
        fname = "Jess"  
        value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
        wrapper = textwrap.TextWrapper(width=65) 
        word_list = wrapper.wrap(text=value)
        string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
        pdf.drawString((190+int(450)/2)-int(string_width)/2, 220, fname)
        # pdf.drawString(400, 220, "Veeru")
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowBold", 12)
        pdf.drawString(60, 175, "Age:23")
        pdf.drawString(150, 175, "Height:5'10")
        pdf.drawString(275, 175, "Weight:68")
        pdf.drawString(60, 125, "Age:23")
        pdf.drawString(150, 125, "Height:5'10")
        pdf.drawString(275, 125, "Weight:68")
        pdf.drawString(60, 75, "Age:23")
        pdf.drawString(150, 75, "Height:5'10")
        pdf.drawString(275, 75, "Weight:68")
        pdf.roundRect(375,
                      55, 400, 150, 6, stroke=1, fill=0)
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowRegular", 14)
        y_axis=175
        for element in word_list:
            pdf.drawString(380,y_axis,element)
            y_axis=y_axis-15
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def oneperpageFL(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="TwoperpageOP.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BL = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'landscape.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=landscape(A4))
    pdf.setTitle("Oneperpage_fourperpage_LookBook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(375, 530, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BL, 75, 75, 700, 450)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1, 1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    for i in range(6):
        pdf.drawImage(fn, 15, 550, 30, 30)
        pdf.line(60, 550, 815, 550)
        pdf.drawImage(ir, 25, 240, 190, 300)
        pdf.drawImage(ir, 225, 240, 190, 300)
        pdf.drawImage(ir, 425, 240, 190, 300)
        pdf.drawImage(ir, 625, 240, 190, 300)
        pdf.setFont("GilroyBold", 14)
        pdf.setFillColor("#ff1552")
        pdf.roundRect(220, 215, 400, 20, 6, stroke=1, fill=1)
        pdf.setFillColor("white")
        fname = "Jess"  
        value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
        wrapper = textwrap.TextWrapper(width=70) 
        word_list = wrapper.wrap(text=value)
        string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
        pdf.drawString((190+int(450)/2)-int(string_width)/2, 220, fname)
        # pdf.drawString(400, 220, "Veeru")
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowBold", 12)
        pdf.drawString(50, 175, "Age:23")
        pdf.drawString(150, 175, "Height:5'10")
        pdf.drawString(275, 175, "Weight:68")
        pdf.drawString(50, 125, "Age:23")
        pdf.drawString(150, 125, "Height:5'10")
        pdf.drawString(275, 125, "Weight:68")
        pdf.drawString(50, 75, "Age:23")
        pdf.drawString(150, 75, "Height:5'10")
        pdf.drawString(275, 75, "Weight:68")
        pdf.roundRect(375,
                      55, 440, 150, 6, stroke=1, fill=0)
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowRegular", 14)
        y_axis=175
        for element in word_list:
            pdf.drawString(380,y_axis,element)
            y_axis=y_axis-15
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def OneperpageOP(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="TwoperpageOP.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BL = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'landscape.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=landscape(A4))
    pdf.setTitle("OneperpageLookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(375, 530, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BL, 75, 75, 700, 450)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1, 1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    for i in range(0, 6):
        pdf.drawImage(fn, 15, 550, 30, 30)
        pdf.line(60, 550, 775, 550)
        pdf.drawImage(ir, 325, 230, 200, 300)
        pdf.setFont("GilroyBold", 14)
        pdf.setFillColor("#ff1552")
        pdf.roundRect(285, 205, 285, 20, 6, stroke=1, fill=1)
        pdf.setFillColor("white")
        fname = "jess"  
        value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
        wrapper = textwrap.TextWrapper(width=60) 
        word_list = wrapper.wrap(text=value)
        string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
        pdf.drawString((285+(int(285)/2))-int(string_width)/2, 210, fname)
        # pdf.drawString(400, 190, "Veeru")
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowBold", 12)
        pdf.drawString(285, 180, "Age:23")
        pdf.drawString(385, 180, "Weight:23")
        pdf.drawString(485, 180, "Height:23")
        pdf.drawString(285, 155, "Age:23")
        pdf.drawString(385, 155, "Weight:23")
        pdf.drawString(485, 155, "Height:23")
        pdf.drawString(285, 130, "Age:23")
        pdf.drawString(385, 130, "Weight:23")
        pdf.drawString(485, 130, "Height:23")
        pdf.roundRect(285,
                      35, 285, 75, 6, stroke=1, fill=0)
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowRegular", 10)
        y_axis=100
        for element in word_list:
            pdf.drawString(290,y_axis,element)
            y_axis=y_axis-15
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def OneperpageTP(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="TwoperpageOP.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BL = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'landscape.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=landscape(A4))
    pdf.setTitle("TwoperpageLookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(375, 530, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BL, 75, 75, 700, 450)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1, 1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    for i in range(3):
        pdf.drawImage(fn, 15, 550, 30, 30)
        pdf.line(60, 550, 775, 550)
        pdf.drawImage(ir, 200, 245, 200, 300)
        pdf.drawImage(ir, 450, 245, 200, 300)
        pdf.setFont("GilroyBold", 14)
        pdf.setFillColor("#ff1552")
        pdf.roundRect(300, 215, 275, 20, 6, stroke=1, fill=1)
        pdf.setFillColor("white")
        # pdf.drawString(415, 280, "veeru")
        fname = "jess"  
        value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
        wrapper = textwrap.TextWrapper(width=85) 
        word_list = wrapper.wrap(text=value)
        string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
        pdf.drawString((285+(int(285)/2))-int(string_width)/2, 220, fname)
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowBold", 12)
        pdf.drawString(200, 190, "Age:23")
        pdf.drawString(390, 190, "Height:5'10")
        pdf.drawString(580, 190, "Weight:68")
        pdf.drawString(200, 170, "Age:23")
        pdf.drawString(390, 170, "Height:5'10")
        pdf.drawString(580, 170, "Weight:68")
        pdf.drawString(200, 150, "Age:23")
        pdf.drawString(390, 150, "Height:5'10")
        pdf.drawString(580, 150, "Weight:68")
        pdf.roundRect(200,
                      50, 455, 75, 6, stroke=1, fill=0)
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowRegular", 12)
        y_axis=100
        for element in word_list:
            pdf.drawString(210,y_axis,element)
            y_axis=y_axis-15
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response


def OneperpageFP(request):
    response = HttpResponse(content_type='application/pdf')
    # d = datetime.today().strftime('%Y-%m-%d')
    # response['Content-Disposition'] = f'inline; filename="TwoperpageOP.pdf"'
    fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'm.jpg')
    BP = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'Potrait.jpg')
    buffer = BytesIO()
    pdf = canvas.Canvas(buffer, pagesize=letter)
    pdf.setTitle("FourperpageLookbook")
    pdf.setFillColorRGB(0, 0, 0)
    pdf.setFont("Courier-Bold", 18)
    pdf.drawString(275, 730, "[Sample]")
    pdf.setFont("Courier", 12)
    pdf.drawImage(BP, 20, 20, 600, 700)
    pdf.showPage()
    res = requests.get(
        "https://mdapi01.maddogcasting.com/cdn-photophoto?id=66868bf08a72cd68713e09f32ab15d71&height=200&width=200")
    Im = Image.open(BytesIO(res.content))
    color = "black"
    border = (1, 1, 1, 1)
    nIm = ImageOps.expand(Im, border=border, fill=color)
    ir = ImageReader(nIm)
    for i in range(0, 6):
        pdf.drawImage(fn, 15, 750, 30, 30)
        pdf.line(60, 750, 600, 750)
        pdf.drawImage(ir, 150, 560, 120, 180)
        pdf.drawImage(ir, 325, 560, 120, 180)
        pdf.drawImage(ir, 150, 370, 120, 180)
        pdf.drawImage(ir, 325, 370, 120, 180)
        pdf.setFont("GilroyBold", 14)
        pdf.setFillColor("#ff1552")
        pdf.roundRect(185, 345, 230, 20, 6, stroke=1, fill=1)
        pdf.setFillColor("white")
        # pdf.drawString(285, 350, "veeru")
        fname = "jess Harding"  
        value = """This function wraps the input paragraph such that each line in the paragraph is at most width characters long. The wrap method returns a list of output lines. The returned list is empty if the wrapped output has no content."""
        wrapper = textwrap.TextWrapper(width=55) 
        word_list = wrapper.wrap(text=value)
        string_width = pdfmetrics.stringWidth(fname,"GilroyBold", 14)
        pdf.drawString((185+(int(230)/2))-int(string_width)/2, 350, fname)
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowBold", 12)
        pdf.drawString(150, 325, "Age:23")
        pdf.drawString(265, 325, "Height:5'10")
        pdf.drawString(385, 325, "Weight:68")
        pdf.drawString(150, 300, "Age:23")
        pdf.drawString(265, 300, "Height:5'10")
        pdf.drawString(385, 300, "Weight:68")
        pdf.drawString(150, 275, "Age:23")
        pdf.drawString(265, 275, "Height:5'10")
        pdf.drawString(385, 275, "Weight:68")
        pdf.roundRect(150,
                      100, 300, 150, 6, stroke=1, fill=0)
        pdf.setFillColorRGB(0, 0, 0)
        pdf.setFont("BarlowRegular", 12)
        y_axis=220
        for element in word_list:
            pdf.drawString(160,y_axis,element)
            y_axis=y_axis-15
        pdf.showPage()
    pdf.save()
    pdfp = buffer.getvalue()
    buffer.close()
    response.write(pdfp)
    return response