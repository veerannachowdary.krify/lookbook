"""crudmixins URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views
from rest_framework.routers import DefaultRouter
urlpatterns = [
    path('admin/', admin.site.urls),
    # path('', views.Studentgetinsert.as_view()),
    # path('student/<int:pk>', views.Studentupdatedel.as_view()),
    path('fourperpageP/', views.FourPerPageP),
    path('fourperpageL/', views.FourPerPageL),
    path('sixperpage/', views.SixPerPage),
    path('twoperpageOL/', views.TwoperpageOL),
    path('twoperpageTL/', views.TwoperpageTL),
    path('twoperpageOP/', views.TwoperpageOP),
    path('twoperpageTP/', views.TwoperpageTP),
    path('threeperpageOL/', views.ThreeperpageOL),
    path('threeperpageTL/', views.ThreeperpageTL),
    path('threeperpageOP/', views.ThreeperpageOP),
    path('oneperpageOL/', views.OneperpageOL),
    path('oneperpageTL/', views.OneperpageTL),
    path('oneperpageThL/',views.OneperpageThL),
    path('oneperpageFL/', views.oneperpageFL),
    path('oneperpageOP/', views.OneperpageOP),
    path('oneperpageTP/', views.OneperpageTP),
    path('oneperpageFP/', views.OneperpageFP)
]